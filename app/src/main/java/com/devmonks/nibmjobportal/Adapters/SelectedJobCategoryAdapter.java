package com.devmonks.nibmjobportal.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.devmonks.nibmjobportal.Model.JobCategoryModel;
import com.devmonks.nibmjobportal.R;

import java.util.List;
import java.util.Locale;

public class SelectedJobCategoryAdapter extends RecyclerView.Adapter<SelectedJobCategoryAdapter.ViewHolder> {

    private final List<JobCategoryModel> jobCategoryList;
    private final Context context;
    private JobCategoryModel jobCategory;
    private OnJobCategoryClickedListener listener;

    public SelectedJobCategoryAdapter(List<JobCategoryModel> jobCategoryList, Context context, OnJobCategoryClickedListener listener) {
        this.jobCategoryList = jobCategoryList;
        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_category, parent, false);
        return new SelectedJobCategoryAdapter.ViewHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        this.jobCategory = jobCategoryList.get(position);
        holder.txtCategoryName.setText(jobCategory.getCategoryName());
        holder.txtJobCount.setText(
                jobCategory.getJobCount() > 100 ? "100+ Jobs"
                : String.format(Locale.ENGLISH, "%d Jobs", jobCategory.getJobCount())
        );
    }

    @Override
    public int getItemCount() {
        return jobCategoryList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        OnJobCategoryClickedListener listener;
        TextView txtCategoryName;
        TextView txtJobCount;

        public ViewHolder(@NonNull View itemView, OnJobCategoryClickedListener listener) {
            super(itemView);
            this.txtCategoryName = itemView.findViewById(R.id.txtCategoryName);
            this.txtJobCount = itemView.findViewById(R.id.txtJobCount);
            this.listener = listener;

            this.itemView.setOnClickListener(v -> {
                this.listener.onJobCategorySelected(getAdapterPosition());
            });
        }
    }

    public interface OnJobCategoryClickedListener {
        void onJobCategorySelected(int position);
    }
}
