package com.devmonks.nibmjobportal.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.devmonks.nibmjobportal.Model.JOBVacancyModel;
import com.devmonks.nibmjobportal.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class JOBVacancyAdapter extends RecyclerView.Adapter<JOBVacancyAdapter.ViewHolder> {

    private  List<JOBVacancyModel> jobVacancyList;
    private final Context context;
    private JOBVacancyModel jobVacancy;
    private OnJobVacancyClickedListener listener;

    public JOBVacancyAdapter(List<JOBVacancyModel> jobVacancyList, Context context, OnJobVacancyClickedListener listener) {
        this.jobVacancyList = jobVacancyList;
        this.context = context;
        this.listener = listener;
    }

    public void jobFilterList(List<JOBVacancyModel> filteredList) {
        jobVacancyList = filteredList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_job_post, parent, false);
        return new JOBVacancyAdapter.ViewHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        this.jobVacancy = jobVacancyList.get(position);
        Picasso.get().load(jobVacancy.getCompanyLogoURL()).into(holder.imgCompanyLogo);
        holder.txtVacancyName.setText(jobVacancy.getVacancyName());
        holder.txtCompanyName.setText(jobVacancy.getCompanyName());
        holder.txtClosingDate.setText(String.format(Locale.ENGLISH, "Closing Date: %s", jobVacancy.getClosingDate()));
    }

    @Override
    public int getItemCount() {
        return jobVacancyList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        OnJobVacancyClickedListener listener;
        ImageView imgCompanyLogo;
        TextView txtVacancyName;
        TextView txtCompanyName;
        TextView txtClosingDate;

        public ViewHolder(@NonNull View itemView, OnJobVacancyClickedListener listener) {
            super(itemView);
            this.listener = listener;
            this.imgCompanyLogo = itemView.findViewById(R.id.imgCompanyLogo);
            this.txtVacancyName = itemView.findViewById(R.id.txtVacancyName);
            this.txtCompanyName = itemView.findViewById(R.id.txtCompanyName);
            this.txtClosingDate = itemView.findViewById(R.id.txtClosingDate);

            itemView.setOnClickListener(v -> {
                this.listener.onJobVacancySelected(getAdapterPosition());
            });
        }
    }

    public interface OnJobVacancyClickedListener {
        void onJobVacancySelected(int position);
    }
}
