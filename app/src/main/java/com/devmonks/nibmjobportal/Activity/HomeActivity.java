package com.devmonks.nibmjobportal.Activity;

import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.EditText;
import android.widget.Toast;

import com.devmonks.nibmjobportal.Adapters.JOBVacancyAdapter;
import com.devmonks.nibmjobportal.Adapters.SelectedJobCategoryAdapter;
import com.devmonks.nibmjobportal.Model.JOBVacancyModel;
import com.devmonks.nibmjobportal.Model.JobCategoryModel;
import com.devmonks.nibmjobportal.R;
import com.devmonks.nibmjobportal.Util.ItemOffsetDecoration;
import com.devmonks.nibmjobportal.databinding.ActivityHomeBinding;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class HomeActivity extends BaseActivity implements SelectedJobCategoryAdapter.OnJobCategoryClickedListener, JOBVacancyAdapter.OnJobVacancyClickedListener {

    //Ui Components
    private EditText mEtSearchOption;


    private ActivityHomeBinding binding;
    private SelectedJobCategoryAdapter mSelectedJobCategoryAdapter;
    private JOBVacancyAdapter mJobVacancyAdapter;
    private List<JobCategoryModel> mJobCategoryList;
    private List<JOBVacancyModel> mJobVacancyList;
    private Animation recyclerAnimation;
    String text;

    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityHomeBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initView();
        setupResources();
    }

    private void initView() {
        mEtSearchOption = findViewById(R.id.activity_home_et_search);
        mEtSearchOption.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void setupResources() {
        recyclerAnimation = AnimationUtils.loadAnimation(this, R.anim.fade_enter);
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(this, R.anim.layout_animation_fall_down);
        final ItemOffsetDecoration categoryItemDecoration = new ItemOffsetDecoration(this, R.dimen._1sdp, R.dimen._1sdp, R.dimen._5sdp, R.dimen._5sdp);
        final ItemOffsetDecoration vacancyItemDecoration = new ItemOffsetDecoration(this, R.dimen._5sdp, R.dimen._5sdp, R.dimen._1sdp, R.dimen._1sdp);

        binding.recyclerMyCategories.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        binding.recyclerMyCategories.addItemDecoration(categoryItemDecoration);
        binding.recyclerMyCategories.setLayoutAnimation(controller);

        binding.recyclerJobs.setLayoutManager(new LinearLayoutManager(this));
        binding.recyclerJobs.addItemDecoration(vacancyItemDecoration);
        binding.recyclerJobs.setLayoutAnimation(controller);

        mJobCategoryList = new ArrayList<>();
        mSelectedJobCategoryAdapter = new SelectedJobCategoryAdapter(mJobCategoryList, this, this);

        mJobVacancyList = new ArrayList<>();
        mJobVacancyAdapter = new JOBVacancyAdapter(mJobVacancyList, this, this);

        binding.recyclerMyCategories.setAdapter(mSelectedJobCategoryAdapter);
        binding.recyclerJobs.setAdapter(mJobVacancyAdapter);

    }


    @Override
    public void onJobCategorySelected(int position) {
        Toast.makeText(this, String.format(Locale.ENGLISH, "Selected %s", mJobCategoryList.get(position).getCategoryName()), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onJobVacancySelected(int position) {

    }
}