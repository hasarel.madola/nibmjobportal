package com.devmonks.nibmjobportal.Activity;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.VibrationEffect;
import android.os.Vibrator;

import androidx.annotation.Nullable;

import com.devmonks.nibmjobportal.R;
import com.tapadoo.alerter.Alerter;

public abstract class BaseActivity extends Activity {

    protected Vibrator vibrator;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
    }

    protected void displayProgress(Activity activity, String title, String message) {
        Alerter.create(activity)
                .setTitle(title)
                .setText(message)
                .enableProgress(true)
                .disableOutsideTouch()
                .setDismissable(false)
                .enableInfiniteDuration(true)
                .setProgressColorRes(R.color.blue_light)
                .show();
    }

    protected void displayProgress(Activity activity) {
        Alerter.create(activity)
                .setTitle("Please wait")
                .setText("Operation in progress")
                .enableProgress(true)
                .disableOutsideTouch()
                .setDismissable(false)
                .enableInfiniteDuration(true)
                .setProgressColorRes(R.color.blue_light)
                .show();
    }

    protected void hideAlerts() {
        Alerter.hide();
    }

    protected void displayAlert(Activity activity, AlertType type, String title, String message) {
        switch (type) {
            case SUCCESS:
                Alerter.create(activity)
                        .enableVibration(true)
                        .setTitle(title)
                        .setText(message)
                        .setBackgroundColorRes(R.color.bg_success)
                        .setIcon(R.drawable.ic_success)
                        .setIconColorFilter(0)
                        .setDuration(2000)
                        .show();
                break;
            case WARNING:
                Alerter.create(activity)
                        .enableVibration(true)
                        .setTitle(title)
                        .setText(message)
                        .setBackgroundColorRes(R.color.bg_warning)
                        .setIcon(R.drawable.ic_warning)
                        .setIconColorFilter(0)
                        .setDuration(2000)
                        .show();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    vibrator.vibrate(VibrationEffect.createOneShot(50, VibrationEffect.DEFAULT_AMPLITUDE));
                } else {
                    vibrator.vibrate(50);
                }
                break;
            case ERROR:
                Alerter.create(activity)
                        .enableVibration(true)
                        .setTitle(title)
                        .setText(message)
                        .setBackgroundColorRes(R.color.bg_error)
                        .setIcon(R.drawable.ic_error)
                        .setIconColorFilter(0)
                        .setDuration(2000)
                        .show();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    vibrator.vibrate(VibrationEffect.createOneShot(100, VibrationEffect.DEFAULT_AMPLITUDE));
                } else {
                    vibrator.vibrate(100);
                }
                break;
        }
    }

    public enum AlertType {
        SUCCESS,
        WARNING,
        ERROR
    }
}
