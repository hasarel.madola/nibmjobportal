package com.devmonks.nibmjobportal.Model;

public class JobCategoryModel {
    private String categoryName;
    private int jobCount;

    public JobCategoryModel(String categoryName, int jobCount) {
        this.categoryName = categoryName;
        this.jobCount = jobCount;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public int getJobCount() {
        return jobCount;
    }

    public void setJobCount(int jobCount) {
        this.jobCount = jobCount;
    }
}
