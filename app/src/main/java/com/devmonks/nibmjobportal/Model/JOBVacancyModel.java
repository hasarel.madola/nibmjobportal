package com.devmonks.nibmjobportal.Model;

public class JOBVacancyModel {
    private String vacancyName;
    private String companyName;
    private String companyLogoURL;
    private String closingDate;

    public JOBVacancyModel(String vacancyName, String companyName, String companyLogoURL, String closingDate) {
        this.vacancyName = vacancyName;
        this.companyName = companyName;
        this.companyLogoURL = companyLogoURL;
        this.closingDate = closingDate;
    }

    public String getVacancyName() {
        return vacancyName;
    }

    public void setVacancyName(String vacancyName) {
        this.vacancyName = vacancyName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyLogoURL() {
        return companyLogoURL;
    }

    public void setCompanyLogoURL(String companyLogoURL) {
        this.companyLogoURL = companyLogoURL;
    }

    public String getClosingDate() {
        return closingDate;
    }

    public void setClosingDate(String closingDate) {
        this.closingDate = closingDate;
    }
}
